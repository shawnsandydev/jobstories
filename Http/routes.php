<?php

Route::group(['middleware' => 'web', 'prefix' => 'jobstories', 'namespace' => 'Modules\JobStories\Http\Controllers'], function()
{
	Route::get('/', 'JobStoriesController@index');
});