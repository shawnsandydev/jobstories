<?php

namespace Modules\Jobstories\Http\Controllers;

use Nwidart\Modules\Routing\Controller;

class JobStoriesController extends Controller {

	public function index()
	{
		return view('jobstories::index');
	}

}
