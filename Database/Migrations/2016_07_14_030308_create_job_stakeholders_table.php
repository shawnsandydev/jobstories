<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobStakeholdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('job_stakeholders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('job_details_id')->nullable();
			$table->string('job_title')->nullable();
			$table->text('job_description', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_stakeholders');
	}

}
